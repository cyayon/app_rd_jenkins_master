# terraform apply/plan/destroy -var-file="yourTFVARFile.tfvars"

# TERRAFORM BLOCK
terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "~> 2.65"
    }
  }
  backend "local" {
    path = "../../_TFSTAT/PRD-RD_Jenkins-master.tfstate"
  }
  required_version = ">= 0.14.9"
}


# PROVIDER BLOCK

provider "azurerm" {
  features {}
  subscription_id = var.SubID
}


# GLOBAL VARIABLES BLOCK

variable SubID {
  type        = string
  description = "the GUID of the targeted Subscription"
#  default     =  "" // defined in tfvar
}

variable Tags {
  type        = map(string)
  description = "A map of the tags to use on the resources that are deployed with this module"
  default = {
      CostCenter      : "BTM29E",
      Environment     : "PROD",
      CreatedBy       : "Luka DUJARDIN",
      OwnerName       : "Cyrille Guillard",
      OwnerTeam       : "R&D - Home & Building",
      Project         : "R&D - Jenkins",
      ApplicationName : "R&D - Jenkins Master",
      Architect       : "Christophe YAYON"
  }
}

variable Location {
  type = string
  default = "westeurope"
}

variable environment {
  type = string
#  default = "" // defined defined in tfvar
}

variable defaultname {
  type = string
#  default = "" // defined defined in tfvar
}

variable RgName {
  type = string
#  default = "" // defined in tfvar
}

variable RgNetworkName {
  type = string
#  default = "" // defined in tfvar
}

variable VnetName {
  type = string
#  default = "" // defined in tfvar
}

variable SubnetName {
  type = string
#  default = "" // defined in tfvar
}

variable SubnetRange {
  type = list
#  default = [""] // defined in tfvar
}

variable AsFaultDomainCount {
  type        = string
  default     = "3" // could be defined in tfvar
}

variable AsUpdateDomainCount {
  type        = string
  default     = "5" // could be defined in tfvar
}

variable VmLinuxName {
  type = list
#  default = [""] // defined in tfvar
}

variable VmLinuxSize {
  type = list
#  default = [""] // defined in tfvar
}

variable MngDisksName {
  type = list
#  default = [""]  // defined in tfvar
}

variable MngDisksSize {
  type = list
#  default = [""]  // defined in tfvar
}

variable "PrivateIPAddr" {
  type        = list
#  default     = [""] // defined in tfvar
}

variable OsDiskType {
  type        = string
  default     = "Standard_LRS" // could be defined in tfvar
}

variable OsDiskCaching {
  type        = string
  default     = "ReadWrite" // cloud be defined in tfvar
}

variable MngDiskType {
  type        = string
  default     = "Standard_LRS" // could be defined in tfvar
}

variable MngDiskCaching {
  type        = string
  default     = "ReadWrite" // cloud be defined in tfvar
}

variable "ShutdownEnabled" {
  type        = string
  default     = false // cloud be defined in tfvar
}

variable "ShutdownTime" {
  type        = string
  default     = "2000" // cloud be defined in tfvar
}

variable "ShutdownTimezone" {
  type        = string
  default     = "Romance Standard Time" // cloud be defined in tfvar
}

variable "ShutdownNotif" {
  type        = string
  default     = false // cloud be defined in tfvar
}

variable adminSSHPub {
  type = string
  description = "Provide administrator SSH pub key path" // cloud be defined in tfvar
}

variable adminUsername {
  type          = string
  sensitive     = true
  description   = "Provide administrator login - from keepass Outside GFI Scope"
}

variable adminPassword {
  type          = string
  sensitive     = true
  description   = "Provide administrator Password - from keepass Outside GFI Scope"
}



# MODULE 
module "Jenkins" {

  # Path
  source = "../../modules/Jenkins"

  # Global Variables
  Tags = var.Tags
  Location = var.Location
  RgName = var.RgName
  RgVnetName = var.RgNetworkName
 
  # AS
  AsName = ["as-${var.defaultname}"]
  AsFaultDomainCount = var.AsFaultDomainCount
  AsUpdateDomainCount = var.AsUpdateDomainCount

  # Networking 
  VnetName = var.VnetName 
  SubnetName = var.SubnetName 
  SubnetRange = var.SubnetRange
  PrivateIPAddr = var.PrivateIPAddr
  
  # Vm Linux
  adminUsername = var.adminUsername
  adminPassword = var.adminPassword
  adminSSHPub = var.adminSSHPub
  VmLinuxName = var.VmLinuxName
  VmLinuxSize = var.VmLinuxSize
  SrcImagePublisher = "canonical"
  SrcImageOffer = "0001-com-ubuntu-server-focal"
  SrcImageSku = "20_04-lts-gen2"
  SrcImageVersion = "latest"
  OsDiskType = var.OsDiskType
  OsDiskCaching = var.OsDiskCaching

  # Managed Disks / Size
  MngDiskType = var.MngDiskType
  MngDiskCaching = var.MngDiskCaching
  MngDisksName = var.MngDisksName
  MngDisksSize = var.MngDisksSize

  # AutoShutdown
  ShutdownEnabled = var.ShutdownEnabled
  ShutdownTime = var.ShutdownTime
  ShutdownTimezone = var.ShutdownTimezone
  ShutdownNotif = var.ShutdownNotif
    
  # TODO :
}

