# Nic Linux
resource "azurerm_network_interface" "nic-linux" {
  count               = length(var.VmLinuxName)
  name                = "${element(var.VmLinuxName, count.index)}-nic"
  location            = var.Location
  resource_group_name = var.RgName
  tags                = var.Tags

  ip_configuration {
    name                          = "${element(var.VmLinuxName, count.index)}-ip"
    subnet_id                     =  data.azurerm_subnet.subnet.id
    private_ip_address_allocation = "Static"
    private_ip_address            = element(var.PrivateIPAddr, (count.index))
  }
}
