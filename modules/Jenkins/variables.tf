
######### TAGS VARIABLES

variable "Tags" {
  type        = map(string)
  description = "A map of the tags to use on the resources that are deployed with this module"
}



######### RG AND CUSTOM VARIABLES
variable "Location" {
  type        = string
  description = "Location"
}

variable "RgName" {
  type        = string
  description = "Application Ressource group name"
}

variable "SubnetName" {
  type        = string
  description = "Application Subnet Name"
}

variable "SubnetRange" {
  description = "Application Subnet Range"
  type        = list
}

variable "VnetName" {
  type        = string
  description = "Application Vnet Name"
}

variable "RgVnetName" {
  type        = string
  description = "Ressource group containing the Vnet"
}


######## AS VARIABLES
variable "AsName" {
  type        = list
  description = "Availibility Set Name"
}

variable "AsFaultDomainCount" {
  type        = string
  description = "Availibility Set Fault Domain Count"
}

variable "AsUpdateDomainCount" {
  type        = string
  description = "Availibility Set Update Domain Count"
}


######### VM VARIABLES
variable "VmLinuxName" {
  type        = list
  description = "Linux Virtual Machine Name"
}

variable "VmLinuxSize" {
  type        = list
  description = "Linux Virtual Machine Size"
}


######## LOGIN / PASSWORD 
variable "adminUsername" {
  type        = string
  sensitive   = true
  description = "Administrator user name"
}

variable "adminPassword" {
  type          = string
  sensitive     = true
  description   = "Administrator Password"
}

variable "adminSSHPub" {
  type        = string
  description ="Administrator SSH pub key"
}



variable "SrcImagePublisher" {
  type = string
  description ="OS Publisher"
}

variable "SrcImageOffer" {
  type = string
  description ="OS Offer"
}

variable "SrcImageSku" {
  type = string
  description ="OS Sku"
}

variable "SrcImageVersion" {
  type = string
  description ="OS Version"
}

variable "OsDiskType" {
  type = string
  description ="OS Disk Type"
}
variable "OsDiskCaching" {
  type = string
  description ="OS Disk Caching"
}


########### NIC VARIABLES

variable "PrivateIPAddr" {
  description = "Static IP that will be created"
  type        = list
}



########### MANAGED DISKS VARIABLES

variable "MngDisksName" {
  type        = list
  description = "Managed Disks Name"
}

variable "MngDisksSize" {
  type        = list
  description = "Managed Disks Size"
}

variable "MngDiskType" {
  type = string
  description ="Managed Disk Type"
}
variable "MngDiskCaching" {
  type = string
  description ="Managed Disk Caching"
}




########### SHUTDOWN

variable "ShutdownEnabled" {
  type        = string
  description = "Shutdown VM"
}

variable "ShutdownTime" {
  type        = string
  description = "Shutdown VM Time"
}

variable "ShutdownTimezone" {
  type        = string
  description = "Shutdown VM Timezone"
}

variable "ShutdownNotif" {
  type        = string
  description = "Shutdown VM Notification"
}

