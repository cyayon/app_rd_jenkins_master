# Autoshutdown
resource "azurerm_dev_test_global_vm_shutdown_schedule" "shutdown-linux" {
  count                 = length(var.VmLinuxName)
  virtual_machine_id    = azurerm_linux_virtual_machine.vm-linux[count.index].id
  location              = var.Location
  tags                  = var.Tags
  enabled               = var.ShutdownEnabled
  daily_recurrence_time = var.ShutdownTime
  timezone              = var.ShutdownTimezone

  notification_settings {
    enabled             = var.ShutdownNotif
  }
}
