resource "azurerm_availability_set" "as" {
  count                         = length(var.AsName)
  name                          = element(var.AsName, count.index)
  managed                       = true
  location                      = var.Location
  resource_group_name           = var.RgName
  platform_fault_domain_count   = var.AsFaultDomainCount
  platform_update_domain_count  = var.AsUpdateDomainCount
  tags                          = var.Tags
}
