# Managed Disks
resource "azurerm_managed_disk" "mhd" {
  count                         = length(var.MngDisksName)
  name                          = element(var.MngDisksName, count.index)
  location                      = var.Location
  resource_group_name           = var.RgName
  tags                          = var.Tags
  storage_account_type          = var.MngDiskType
  create_option                 = "Empty"
  disk_size_gb                  = "${element(var.MngDisksSize, (count.index))}"
  public_network_access_enabled = false // TBD
}

resource "azurerm_virtual_machine_data_disk_attachment" "mhd" {
  count                     = length(var.MngDisksName)
  managed_disk_id           = azurerm_managed_disk.mhd[count.index].id
  virtual_machine_id        = azurerm_linux_virtual_machine.vm-linux[count.index].id
  lun                       = "10"
  caching                   = var.MngDiskCaching
}
