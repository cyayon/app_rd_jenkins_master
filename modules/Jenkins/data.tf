data "azurerm_resources" "rg" {
  name = "${var.RgName}"
}

data "azurerm_subnet" "subnet" {
  virtual_network_name  = "${var.VnetName}"
  name                  = "${var.SubnetName}"
  resource_group_name   = "${var.RgVnetName}"
}
