# Create linux vm
resource "azurerm_linux_virtual_machine" "vm-linux" {
  count                 = length(var.VmLinuxName)
  name                  = element(var.VmLinuxName, (count.index))
  resource_group_name   = var.RgName
  location              = var.Location
  tags                  = var.Tags
  size                  = element(var.VmLinuxSize, (count.index))
  admin_username        = var.adminUsername
  admin_password        = var.adminPassword
  availability_set_id   = azurerm_availability_set.as[0].id
  network_interface_ids = [azurerm_network_interface.nic-linux[count.index].id]


  os_disk {
    name                      = "${element(var.VmLinuxName, (count.index))}-disk"
    caching                   = var.OsDiskCaching
    storage_account_type      = var.OsDiskType
  }

  admin_ssh_key {
    username   = var.adminUsername
    public_key = file("${var.adminSSHPub}")
  }

  source_image_reference {
    publisher = var.SrcImagePublisher
    offer     = var.SrcImageOffer
    sku       = var.SrcImageSku
    version   = var.SrcImageVersion
  }

  depends_on = [
    azurerm_network_interface.nic-linux[0],
  ]
}
